# Laravel 5.8 Simple CRUD

<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## Installation

#### Clone this project in your work folder and open the directory:

```bash
git clone https://gitlab.com/carles.mr.19/laravel-simple-crud.git
```

#### Open the "laravel-simple-crud" folder:

```bash
cd laravel-simple-crud
```

#### Download Project Dependencies

As the project dependencies are handled by ** composer ** we have to execute the order:

```bash
composer install
```

### Important!

You must create a database before proceeding with the project configuration process.

#### Configure Environment

The configuration of the environment is done in the **.env** file but this file can not be modified according to the restrictions of the **.gitignore** file, there is also an example file in the project **.env.example** we have to copy with the following command:

```bash
cp .env.example .env
```

Then we need to modify the values ​​of the environment variables to adapt the configuration to our development environment, such as the connection parameters in the database.

```bash
DB_CONNECTION=mysql
DB_HOST= //IP address of the database
DB_PORT=3306
DB_DATABASE= //Name of the database
DB_USERNAME= //User
DB_PASSWORD= //Password
```

#### Generate Application Security Key

```bash
php artisan key:generate
```

#### Migrate the database

The project already has the models, migrations and seeders generated. Then the only thing we need is to execute the migration and execute the following command:

```bash
php artisan migrate --seed
```

#### Start the application

```bash
php artisan serve
```

Finally we will have to enter the "localhost: 8000" address in our browser to see the application.

---

Carles Miranda Rodriguez

[Linkedin](https://www.linkedin.com/in/carles-miranda/)

---
