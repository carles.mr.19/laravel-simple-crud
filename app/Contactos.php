<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactos extends Model
{
    protected $fillable = ['nombre', 'email', 'fecha_nacimiento', 'genero'];
}
