<?php

namespace App\Http\Controllers;

use App\Contactos;
use App\Http\Requests\FormContacto;
use Illuminate\Http\Request;

class ContactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactos = Contactos::paginate(10);

        return view('contactos.index')->with('contactos', $contactos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contactos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormContacto $request)
    {
        $validateData = $request->validated();

        $contacto = new Contactos($validateData);
        $contacto->save();

        return redirect()->route('contactos.index')->with('success', 'Se ha añadido el contacto correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contacto = Contactos::findOrFail($id);
        return view('contactos.edit')->with('contacto', $contacto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormContacto $request, $id)
    {

        $validateData = $request->validated();

        $contacto = Contactos::findOrFail($id);
        $contacto->update($validateData);

        return redirect()->route('contactos.index')->with('success', 'Se ha modificado el contacto correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contacto = Contactos::findOrFail($id);
        $contacto->delete();

        return redirect()->route('contactos.index')->with('success', 'Se ha eliminado el contacto correctamente');
    }
}
