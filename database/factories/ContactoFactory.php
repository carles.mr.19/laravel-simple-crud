<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Contactos::class, function (Faker $faker) {
    $gender = $faker->randomElement(['Hombre', 'Mujer']);
    return [
        'nombre' => $faker->name,
        'email' => $faker->email,
        'fecha_nacimiento'=> $faker->date($format = 'Y-m-d', $max = 'now') ,
        'genero' => $gender
    ];
});
