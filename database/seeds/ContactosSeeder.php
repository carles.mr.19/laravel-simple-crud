<?php

use Illuminate\Database\Seeder;
use App\Contactos;

class ContactosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Contactos::class, 20)->create();
    }
}
