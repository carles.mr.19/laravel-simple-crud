@extends('layout.default')

@section('content')
    <div class="card">
        <div class="card-header">Añadir Contacto</div>
        <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{{route('contactos.store')}}">
                @csrf

                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="text" class="form-control" name="nombre">
                </div>

                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" class="form-control" name="email"/>
                </div>

                <div class="form-group">
                    <label for="fecha_nacimiento">Fecha Nacimiento:</label>
                    <input type="date" class="form-control" name="fecha_nacimiento" max="{{date('Y-m-d')}}">
                </div>

                <div class="form-group">
                    <label for="genero">Genero:</label>
                    <select class="form-control" name="genero">
                        <option> </option>
                        <option value="Hombre">Hombre</option>
                        <option value="Mujer">Mujer</option>
                    </select>
                </div>

                <a href="{{route('contactos.index')}}" class="btn btn-danger">Atras</a>
                <button type="submit" class="btn btn-primary">Añadir Contacto</button>
            </form>
        </div>
    </div>
@endsection