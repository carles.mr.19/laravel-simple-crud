@extends('layout.default')

@section('content')
    <div class="card">
        <div class="card-header">Actualizar Contacto</div>
        <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{{route('contactos.update', $contacto->id)}}">
                @csrf
                @method('PATCH')

                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="text" class="form-control" name="nombre" value="{{$contacto->nombre}}">
                </div>

                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" class="form-control" name="email" value="{{$contacto->email}}">
                </div>

                <div class="form-group">
                    <label for="fecha_nacimiento">Fecha Nacimiento:</label>
                    <input type="date" class="form-control" name="fecha_nacimiento" max="{{date('Y-m-d')}}" value="{{ date('Y-m-d', strtotime($contacto->fecha_nacimiento)) }}">
                </div>

                <div class="form-group">
                    <label for="genero">Genero:</label>
                    <select class="form-control" name="genero">
                        <option value="Hombre" @if($contacto->genero=="Hombre") selected='selected' @endif> Hombre </option>
                        <option value="Mujer" @if($contacto->genero=='Mujer') selected='selected' @endif> Mujer </option>
                    </select>
                </div>

                <a href="{{route('contactos.index')}}" class="btn btn-danger">Atras</a>
                <button type="submit" class="btn btn-primary">Actualizar Contacto</button>
            </form>
        </div>
    </div>
@endsection