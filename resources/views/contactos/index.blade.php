@extends('layout.default')

@section('content')

        <div class="float-right">
            <a href="{{route('contactos.create')}}" class="btn btn-primary">Añadir</a>
        </div>
        <h1 style="font-size: 2.2rem">Lista de Contactos</h1>
        <hr/>

        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br />
        @endif

        <table class="table table-bordered bg-light">
            <thead class="bg-dark" style="color: white">
            <tr>
                <th width="60px" style="text-align: center">ID</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Fecha Nacimiento</th>
                <th>Genero</th>
                <th width="150px">Accion</th>
            </tr>
            </thead>
            <tbody>
            @foreach($contactos as $contacto)
                <tr>
                    <th style="text-align: center">{{$contacto->id}}</th>
                    <td>{{$contacto->nombre}}</td>
                    <td>{{$contacto->email}}</td>
                    <td>{{ date('d - M - y', strtotime($contacto->fecha_nacimiento)) }}</td>
                    <td>{{$contacto->genero}}</td>
                    <td align="center">
                        <a class="btn btn-primary btn-sm float-left" title="Edit" href="{{route('contactos.edit',$contacto->id)}}"> Editar </a>

                        <form action="{{ route('contactos.destroy', $contacto->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm float-left" type="submit">Eliminar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <nav>
            <ul class="pagination justify-content-end">
                {{$contactos->links()}}
            </ul>
        </nav>
@endsection