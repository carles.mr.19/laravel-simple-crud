<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Contactos</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    @yield('css')
</head>
<body>
    <div class="container mt-5">
        @yield('content')
    </div>

    <script src="{{asset('js/app.js')}}"></script>
    @yield('js')
</body>
</html>
