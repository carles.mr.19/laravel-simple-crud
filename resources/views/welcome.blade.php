<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>CRUD Simple</title>

        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md"> CRUD Simple </div>
                <div class="subtitle m-b-md"> Creado por Carles Miranda </div>

                <div class="links">
                    <a href="https://www.linkedin.com/in/carles-miranda/">Linkedin</a>
                </div>

                <h1><a href="contactos"> Ir a la APP </a></h1>
            </div>
        </div>
    </body>
</html>
